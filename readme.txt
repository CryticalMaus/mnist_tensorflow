Requirements:
	Python 3.5
	Tensorflow (and subdependancies)
	The four datasets from http://yann.lecun.com/exdb/mnist/

Instructions:
copy the directory path to the datasets into the settings.txt file
execute main.py

This model reliably achieves 98% accuracy on the test dataset
import struct
import numpy as np


def load_MNIST(path_img, path_lbl):
    with open(path_lbl, 'rb') as label_file:
        magic, size = struct.unpack(">II", label_file.read(8))
        if magic != 2049:
            raise ValueError('Magic number mismatch, expected 2049,'
                             'got {}'.format(magic))

        labels = np.fromstring(label_file.read(), dtype=np.uint8)

    with open(path_img, 'rb') as file:
        magic, size, rows, cols = struct.unpack(">IIII", file.read(16))
        if magic != 2051:
            raise ValueError('Magic number mismatch, expected 2051,'
                             'got {}'.format(magic))

        image_data = np.fromstring(file.read(), dtype=np.uint8)
        images = image_data.reshape(size, 28, 28).astype(np.float32) / 255


    return images, labels
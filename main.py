import data
import tensorflow as tf
import tensorflow.contrib.layers as layers
import random
import sys


if sys.version_info[0] < 3:
    raise "Must be using Python 3"


def build_model(batch_size: int, num_labels: int, learning_rate: float):
    with tf.name_scope(name="inputs"):
        X = tf.placeholder(shape=[batch_size, 28, 28], dtype=tf.float32, name="Input")
        Y = tf.placeholder(shape=[batch_size], dtype=tf.int64, name="ExpectedOutput")
        phase = tf.placeholder(tf.bool, name='phase')

    with tf.name_scope(name="preprocess_inputs"):
        shaped_input = tf.reshape(X, [batch_size, 28, 28, 1])
        expected_output = tf.one_hot(indices=tf.cast(Y, tf.int32), depth=num_labels, on_value=1, off_value=0)

    with tf.name_scope(name="convolve_layer_1"):
        reduced_image_x2 = tf.layers.conv2d(
            inputs=shaped_input,
            filters=32,
            kernel_size=[5, 5],
            padding="same",
            activation=tf.nn.relu)
        reduced_image_x2 = tf.layers.max_pooling2d(inputs=reduced_image_x2, pool_size=[2, 2], strides=2)

    with tf.name_scope(name="convolve_layer_2"):
        reduced_image_x4 = tf.layers.conv2d(
            inputs=reduced_image_x2,
            filters=64,
            kernel_size=[5, 5],
            padding="same",
            activation=tf.nn.relu)
        reduced_image_x4 = tf.layers.max_pooling2d(inputs=reduced_image_x4, pool_size=[2, 2], strides=2)

    flat_image = layers.flatten(reduced_image_x4, scope="flat_image")

    with tf.name_scope(name="fully_connected_1"):
        layer_1 = tf.layers.dense(inputs=flat_image, units=1024, activation=tf.nn.relu)
        layer_1 = tf.layers.dropout(inputs=layer_1, rate=0.4, training=phase)

    with tf.name_scope(name="fully_connected_2"):
        layer_out = tf.layers.dense(inputs=layer_1, units=num_labels)

    with tf.name_scope(name="entropy", values=[expected_output, layer_out]) as scope:
        cross_entropy = tf.losses.softmax_cross_entropy(onehot_labels=expected_output, logits=layer_out)
        tf.summary.scalar("error", cross_entropy)

    with tf.name_scope(name="accuracy", values=[expected_output, layer_out]) as scope:
        correct_prediction = tf.equal(tf.argmax(expected_output, 1), tf.argmax(layer_out, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        tf.summary.scalar("accuracy", accuracy)

    with tf.name_scope(name="trainer", values=[cross_entropy]) as scope:
        net_trainer = layers.optimize_loss(
            loss=cross_entropy,
            global_step=tf.contrib.framework.get_global_step(),
            learning_rate=learning_rate,
            optimizer="SGD")

    return X, Y, net_trainer, accuracy, phase


# Process settings file
with open("./settings.txt") as settings_file:
    def process_line(line: str):
        return line[line.find("=") + 1:].strip()

    dataset_path = process_line(settings_file.readline())

    train_images, train_labels = data.load_MNIST(dataset_path + "\\train-images.idx3-ubyte",
                                                 dataset_path + "\\train-labels.idx1-ubyte")
    test_images, test_labels = data.load_MNIST(dataset_path + "\\t10k-images.idx3-ubyte",
                                               dataset_path + "\\t10k-labels.idx1-ubyte")
    batch_size = int(process_line(settings_file.readline()))
    learning_rate = float(process_line(settings_file.readline()))

num_labels = 10
data_size = int(len(train_labels) / batch_size)

#construct model
X, Y, trainer, accuracy, phase = build_model(batch_size, num_labels, learning_rate)

print("Beginning training")
with tf.Session() as session:
    session.run(tf.global_variables_initializer())
    merged_summary = tf.summary.merge_all()
    writer = tf.summary.FileWriter("./graphs/3")
    writer.add_graph(session.graph)

    for i in range(0, data_size):
        label_batch = train_labels[i * batch_size:(i + 1) * batch_size]
        image_batch = train_images[i * batch_size:(i + 1) * batch_size]

        _, sum = session.run([trainer, merged_summary], feed_dict={X: image_batch, Y: label_batch, phase: True})
        writer.add_summary(sum)

        if i % 100 == 0:
            x_sub, y_sub = zip(*random.sample(list(zip(test_images, test_labels)), batch_size))
            print("random sample accuracy",
                  "{:10.1f}%,".format(accuracy.eval(feed_dict={X: x_sub, Y: y_sub, phase: False}) * 100),
                  "{:10.1f}% complete".format(float(i / data_size) * 100))

    acc = 0.0
    for j in range(0, int(len(test_labels) / batch_size)):
        x_sub = test_images[j*batch_size:(j+1)*batch_size]
        y_sub = test_labels[j*batch_size:(j+1)*batch_size]
        acc += accuracy.eval(feed_dict={X: x_sub, Y: y_sub, phase: False})

    acc /= int(len(test_labels) / batch_size)
    print("Training Set Accuracy: " + "{:10.1f}%".format(acc * 100))